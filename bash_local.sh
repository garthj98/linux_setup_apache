# Set IP adress
#IPADRESS=52.215.189.61
read -p 'IP Adress: ' IPADRESS
# Key loacation
KEYLOC=~/.ssh/ch9_shared.pem
# Set Users name 
USER=ec2-user
# Set website folder
WEBFOLDER=~/code/2_week/linux_setup_apache/website
# Set virtual script loaction
SCRPTLOC=~/code/2_week/linux_setup_apache/bash_virtual.sh

# Change permisions to make sure it is executable 
chmod +x bash_virtual.sh 

# Send bash script
scp -i $KEYLOC $SCRPTLOC $USER@$IPADRESS:~/

# Send website folder
scp -i $KEYLOC -r $WEBFOLDER $USER@$IPADRESS:~/

# Run bash script
ssh -i $KEYLOC $USER@$IPADRESS ./bash_virtual.sh  

open http://$IPADRESS/

