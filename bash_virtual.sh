# Update redhat
sudo yum update -y

# Install apache
sudo yum install -y httpd.x86_64

# Start apache
sudo systemctl start httpd.service

sudo systemctl enable httpd.service

# Move index file
sudo mv ~/website/index.html /var/www/html/ 
sudo mv ~/website/redhat.jpg /var/www/html/ 

# Restart apache
sudo systemctl restart httpd.service