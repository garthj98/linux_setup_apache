# How to automatically run and set up apache on an Redhat virtual machine

### Things you will need -
- A redhat virtual machine 
- a secure shell key
- bitbucket repository
- a code editor (like vs code)

## Steps
- save the two scripts `bash_local.sh` and `bash_virtual.sh` in a folder
- produce a folder with a `index.html` file
- Set all bash script to executable `$ chmod +x <script_name>`
- Update variables on bash local script 
- Run the local bash script `./bash_local.sh`

### Local bash script
- Set variables of IP address, users and filepath names in `bash_local.sh`
- securely copies `$ scp` the bash script to be uploaded to the virtual machine and the folder containing the .http file 
- Runs the bash script on the virtual machine

```bash
# Set IP adress
IPADRESS=52.215.189.61
#read -p 'IP Adress: ' $IPADRESS
# Key loacation
KEYLOC=~/.ssh/ch9_shared.pem
# Set Users name 
USER=ec2-user
# Set website folder
WEBFOLDER=~/code/2_week/linux_setup_apache/website
# Set virtual script loaction
SCRPTLOC=~/code/2_week/linux_setup_apache/bash_virtual.sh

# Change permisions to make sure it is executable 
chmod +x bash_virtual.sh 

# Send bash script
scp -i $KEYLOC $SCRPTLOC $USER@$IPADRESS:~/

# Send website folder
scp -i $KEYLOC -r $WEBFOLDER $USER@$IPADRESS:~/

# Run bash script
ssh -i $KEYLOC $USER@$IPADRESS ./bash_virtual.sh  
```

#### VM bash script
- updates the virtual machine
- installs and starts apache
- moves the index.html file to /var/www/html/ 
- restarts apache

```bash
# Update redhat
sudo yum update -y

# Install apache
sudo yum install -y httpd.x86_64

# Start apache
sudo systemctl start httpd.service

sudo systemctl enable httpd.service

# Move index file
sudo mv ~/website/index.html /var/www/html/ 

# Restart apache
sudo systemctl restart httpd.service
```